<?php

namespace App\Actions;

use Throwable;
use App\Models\User;
use RuntimeException;
use App\Models\Project;
use Illuminate\Support\Str;
use App\Contracts\CreatesProjects;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Symfony\Component\Process\Process;

class CreateProject implements CreatesProjects
{
    /**
     * Default directory where all project applications are created.
     *
     * @var string
     */
    protected $projectsDirectory = 'projects';

    /**
     * Create project for given user using given details.
     *
     * @param \App\Models\User $user
     * @param array            $data
     *
     * @return \App\Models\Project
     */
    public function create(User $user, array $data): Project
    {
        return DB::transaction(function () use ($user, $data) {
            return tap($this->createProjectDetails($user, $data), function (Project $project) {
                dispatch(function () use ($project) {
                    $this->createPhysicalStructure($project);
                })->catch(function (Throwable $e) use ($project) {
                    File::deleteDirectory($project->path);

                    $project->delete();

                    app('log')->error($e->getMessage());
                })->onQueue('default');
            });
        });
    }

    /**
     * Create and save project details to database.
     *
     * @param \App\Models\User $user
     * @param array            $data
     *
     * @return \App\Models\Project
     */
    protected function createProjectDetails(User $user, array $data): Project
    {
        $slug = Str::slug(strtolower($data['name']));

        return $user->projects()->create([
            'name' => $data['name'],
            'slug' => $slug,
            'url' => "http://{$slug}.test",
            'description' => $data['description'],
            'has_git' => $data['has_git'],
        ]);
    }

    /**
     * Create physical structure of project and install dependencies.
     *
     * @param \App\Models\Project $project
     *
     * @return void
     *
     * @throws \RuntimeException
     */
    protected function createPhysicalStructure(Project $project): void
    {
        $this->verifyApplicationDoesntExist($this->createFullPathTo($project));

        if ($this->runShellCommands($project) !== 0) {
            throw new RuntimeException('Shell command failed to finish successfully');
        }

        $this->prepareApplication($project->path);
    }

    /**
     * Create full path to project directory.
     *
     * @param \App\Models\Project $project
     *
     * @return string
     */
    protected function createFullPathTo(Project $project): string
    {
        $project->forceFill([
            'path' => storage_path(
                $this->projectsDirectory . DIRECTORY_SEPARATOR . $project->slug
            ),
        ])->save();

        return $project->fresh()->path;
    }

    /**
     * Determine if the application already exists in the projects directory.
     *
     * @param string $directory
     *
     * @return void
     *
     * @throws \RuntimeException
     */
    protected function verifyApplicationDoesntExist(string $directory): void
    {
        if (is_dir($directory) || is_file($directory)) {
            throw new RuntimeException('Application already exists!');
        }
    }

    /**
     * Run project installation shell commands.
     *
     * @param \App\Models\Project $project
     *
     * @return int
     */
    protected function runShellCommands(Project $project): int
    {
        $commands = [
            $this->findComposer() . ' create-project laravel/laravel ' . $project->path . ' --remove-vcs --prefer-dist',
        ];

        if ($project->has_git) {
            $commands = array_merge($commands, [
                'cd ' . $project->path,
                'git init',
                'git add .',
                'git commit -m \'Initial commit\'',
            ]);
        }

        return $this->runProcess($commands);
    }

    /**
     * Get the composer command for the environment.
     *
     * @return string
     */
    protected function findComposer(): string
    {
        $composerPath = getcwd() . '/composer.phar';

        if (file_exists($composerPath)) {
            return '"' . PHP_BINARY . '" ' . $composerPath;
        }

        return 'composer';
    }

    /**
     * Prepare newly created application for development.
     *
     * @param string $directory
     *
     * @return int
     */
    protected function prepareApplication(string $directory): int
    {
        return $this->runProcess([
            'cd ' . $directory,
            'php artisan key:generate',
        ]);
    }

    /**
     * Run given commands as shell script.
     *
     * @param array $commands
     *
     * @return int
     */
    protected function runProcess(array $commands): int
    {
        $process = Process::fromShellCommandline(implode(' && ', $commands), null, null, null, null);

        $process->run();

        return $process->getExitCode();
    }
}
