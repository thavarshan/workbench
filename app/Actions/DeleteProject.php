<?php

namespace App\Actions;

use App\Models\Project;
use App\Contracts\DeletesProjects;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class DeleteProject implements DeletesProjects
{
    /**
     * Delete given project details and physical structure of project.
     *
     * @param \App\Models\Project $project
     *
     * @return void
     */
    public function delete(Project $project): void
    {
        DB::transaction(function () use ($project) {
            File::deleteDirectory($project->path);

            $project->delete();
        });
    }
}
