<?php

namespace App\Contracts;

use App\Models\User;
use App\Models\Project;

interface CreatesProjects
{
    /**
     * Create project for given user using given details.
     *
     * @param \App\Models\User $user
     * @param array            $data
     *
     * @return \App\Models\Project
     */
    public function create(User $user, array $data): Project;
}
