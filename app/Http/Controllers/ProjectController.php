<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Contracts\CreatesProjects;
use App\Contracts\DeletesProjects;
use App\Http\Requests\ProjectRequest;

class ProjectController extends Controller
{
    /**
     * Get all projects that belong to the user.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Models\Project
     */
    public function index(Request $request)
    {
        return $request->user()->projects;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\ProjectRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreatesProjects $creator, ProjectRequest $request)
    {
        $project = $creator->create(auth()->user(), $request->validated());

        return response($project, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Project $project
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project      $project
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Contracts\DeletesProjects $deletor
     * @param \App\Models\Project            $project
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeletesProjects $deletor, Project $project)
    {
        $this->authorize('manage', $project);

        $deletor->delete($project);

        return response([], 204);
    }
}
