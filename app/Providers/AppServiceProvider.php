<?php

namespace App\Providers;

use App\Actions\CreateProject;
use App\Actions\DeleteProject;
use App\Contracts\CreatesProjects;
use App\Contracts\DeletesProjects;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CreatesProjects::class, CreateProject::class);
        $this->app->singleton(DeletesProjects::class, DeleteProject::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
