<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createDefaultUser();
    }

    /**
     * Create default user account.
     *
     * @return \DefaultUserSeeder
     */
    protected function createDefaultUser()
    {
        $this->user = User::create(config('defaults.user'));

        return $this;
    }
}
