<?php

return [
    /*
     * Default/Admin User Details.
     */
    'user' => [
        'username' => 'Thavarshan',
        'name' => 'Thavarshan Thayananthajothy',
        'email' => 'tjthavarshan@gmail.com',
        'email_verified_at' => now(),
        'password' => '$2y$10$8jakkFVc8175VAOGK5Jt/uDT4R9KEwJPdG5jEEceaxCHwyfhkLs2S', // alphaxion77
        'remember_token' => 'Wdd5eAC4tFBrM0c4qT1b1yGrePdlBzONsndKxjEx',
    ],
];
