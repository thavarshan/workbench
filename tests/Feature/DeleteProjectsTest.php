<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Project;
use Illuminate\Support\Facades\File;

class DeleteProjectsTest extends TestCase
{
    protected function tearDown(): void
    {
        File::deleteDirectory(storage_path('projects/my-project'));
    }

    /** @test */
    public function guests_cannot_delete_projects()
    {
        $project = create(Project::class);

        $this->deleteJson('/projects/' . $project->id)->assertStatus(401);

        $this->signIn();

        $this->deleteJson('/projects/' . $project->id)->assertStatus(403);
    }

    /** @test */
    public function only_project_owner_can_delete_their_own_project()
    {
        $user = $this->signIn();

        $project = create(Project::class, ['user_id' => $user->id]);

        $this->deleteJson('/projects/' . $project->id)->assertStatus(204);
    }

    /** @test */
    public function deleteing_projects_also_deletes_project_application()
    {
        $this->signIn();

        $this->postJson('/projects', [
            'name' => 'my-project',
            'description' => 'My super awesome project.',
            'has_git' => false,
        ])->assertStatus(200);

        $project = Project::whereName('my-project')->first();

        $this->assertTrue(File::exists(storage_path('projects/my-project')));

        $this->deleteJson('/projects/' . $project->id)->assertStatus(204);

        $this->assertFalse(File::exists(storage_path('projects/my-project')));
    }
}
