<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Queue;

class CreateProjectsTest extends TestCase
{
    protected function tearDown(): void
    {
        File::deleteDirectory(storage_path('projects/my-project'));
    }

    /** @test */
    public function only_authenticated_users_can_create_projects()
    {
        $response = $this->postJson('/projects');

        $response->assertStatus(401);
    }

    /** @test */
    public function authentiated_users_can_create_projects()
    {
        $this->withoutExceptionHandling();

        $this->signIn();

        Queue::fake();

        Queue::assertNothingPushed();

        $response = $this->postJson('/projects', [
            'name' => 'my-project',
            'description' => 'My super awesome project.',
            'has_git' => false,
        ]);

        $response->assertStatus(200);
    }

    /** @test */
    public function it_creates_the_physical_structure_of_the_project()
    {
        $response = $this->actingAs(create(User::class))
            ->postJson('/projects', [
                'name' => 'my-project',
                'description' => 'My super awesome project.',
                'has_git' => false,
            ]);

        tap($response, function ($response) {
            $response->assertStatus(200);

            $this->assertDatabaseHas('projects', [
                'name' => 'my-project',
                'description' => 'My super awesome project.',
                'has_git' => false,
            ]);
        });

        $this->assertTrue(File::exists(storage_path('projects/my-project')));
    }

    /** @test */
    public function it_creates_the_project_and_initializes_git()
    {
        $response = $this->actingAs(create(User::class))
            ->postJson('/projects', [
                'name' => 'my-project',
                'description' => 'My super awesome project.',
                'has_git' => true,
            ]);

        tap($response, function ($response) {
            $response->assertStatus(200);

            $this->assertDatabaseHas('projects', [
                'name' => 'my-project',
                'description' => 'My super awesome project.',
                'has_git' => true,
            ]);
        });

        $this->assertTrue(File::exists(storage_path('projects/my-project')));
        $this->assertTrue(File::exists(storage_path('projects/my-project/.git')));
    }
}
