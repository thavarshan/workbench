<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Project;

class ProjectTest extends TestCase
{
    /** @test */
    public function it_belongs_to_a_user()
    {
        $project = create(Project::class);

        $this->assertInstanceOf(User::class, $project->user);
    }

    /** @test */
    public function it_has_a_set_of_required_attributes()
    {
        $project = create(Project::class);

        $this->assertDatabaseHas('projects', [
            'name' => $project->name,
            'description' => $project->description,
            'has_git' => $project->has_git,
        ]);
    }
}
