<?php

namespace Tests\Concerns;

use App\Models\User;

trait CreatesFakeUser
{
    /**
     * Mock authenticated user.
     *
     * @param \App\Models\User $user
     *
     * @return \App\Models\User
     */
    protected function signIn(?User $user = null): User
    {
        $user = $user ?: create(User::class);

        $this->actingAs($user);

        return $user;
    }
}
