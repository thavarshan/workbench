@extends('layouts.web.base')

@section('content')
    <section class="py-12 bg-gray-100">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <projects user="{{ auth()->user() }}"></projects>
                </div>
            </div>
        </div>
    </section>
@endsection
