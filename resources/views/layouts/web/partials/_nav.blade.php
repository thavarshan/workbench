<nav class="h-16 flex items-center {{ $bgNav ?? 'bg-white' }} border-b border-gray-200">
    <div class="flex-1 container">
        <div class="flex justify-between items-center">
            <a class="block h-6 w-auto inline-flex items-center" href="/" title="{{ config('app.name') }}">
                <img class="h-6 w-6" src="{{ asset('img/logo.svg') }}" alt="{{ config('app.name') }}">

                <span class="ml-3 font-bold text-lg text-gray-800">Workbench</span>
            </a>

            <div class="ml-10 hidden md:flex flex-1 items-center justify-between">
                <ul class="flex items-center">

                </ul>

                <ul class="flex items-center">
                    <li class="ml-6">
                        <a class="btn btn-primary leading-9" href="#" data-toggle="modal" data-target="#newProjectModal">
                            <span class="mr-1">&plus;</span>

                            <span>{{ __('New project') }}</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="dropdown block md:hidden">

            </div>
        </div>
    </div>
</nav>
