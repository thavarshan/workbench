<footer class="py-16">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div>
                    <span class="text-sm text-gray-500">&copy; {{ date('Y') }} {{ config('app.name') }}</span>
                </div>
            </div>
        </div>
    </div>
</footer>
