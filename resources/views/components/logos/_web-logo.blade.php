<a class="block h-10 w-10" href="/" title="{{ config('app.name') }}">
    {{ $slot }}
</a>
