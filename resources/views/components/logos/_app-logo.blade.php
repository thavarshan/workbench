<a class="block flex-shrink-0 h-8 w-auto" href="/" title="{{ config('app.name') }}">
    {{ $slot }}
</a>
