<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProjectController;

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['verify' => false]);

Route::group([
    'middleware' => 'auth',
], function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::resource('projects', ProjectController::class, [
        'except' => ['create', 'edit'],
    ]);

    Route::get('/projects', [ProjectController::class, 'index']);
});
